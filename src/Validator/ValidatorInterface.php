<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 13.07.17
 * Time: 8:12
 */

namespace KeepSolid\TestTask\Validator;


interface ValidatorInterface
{
    public function isValid() : bool;
    public function validationErrorMessage() : string;
}