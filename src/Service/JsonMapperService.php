<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 13.07.17
 * Time: 3:34
 */

namespace KeepSolid\TestTask\Service;


use KeepSolid\TestTask\Validator\ValidatorInterface;

class JsonMapperService
{
    const NAMES_MAP = [
        'address' => 'adr',
        'homepage' => 'url',
        'arrivalDate' => 'from',
        'departureDate' => 'to',
    ];

    const VALIDATORS_NS = "KeepSolid\\TestTask\\Validator\\";

    /**
     * From phpunit/src/Util/Test.php
     *
     * @param string $docblock
     *
     * @return array
     */
    protected static function parseAnnotations(string $docblock) : array
    {
        $annotations = [];
        // Strip away the docblock header and footer to ease parsing of one line annotations
        $docblock = \substr($docblock, 3, -2);
        if (\preg_match_all('/@(?P<name>[A-Za-z_-]+)(?:[ \t]+(?P<value>.*?))?[ \t]*\r?$/m', $docblock, $matches)) {
            $numMatches = \count($matches[0]);
            for ($i = 0; $i < $numMatches; ++$i) {
                $annotations[$matches['name'][$i]][] = (string) $matches['value'][$i];
            }
        }
        return $annotations;
    }

    /**
     * Returns type of Entity property
     *
     * @param array $annotations
     * @return array
     */
    protected static function inspectProperty(array $annotations) : array
    {
        $type = $annotations['var'][0];
        $isArrayProperty = strpos($type, '[]') !== false;
        if ($isArrayProperty) {
            $type = str_replace('[]', '', $type);
        }
        return [$type, $isArrayProperty];
    }

    /**
     * Converts JSON value to property type
     *
     * @param string $type
     * @param $value
     * @return \DateTime|float|string
     */
    protected static function valueToType(string $type, $value)
    {
        switch ($type) {
            case 'float':
                $value = (float) $value;
                break;
            case '\DateTime':
                $value = new \DateTime($value);
                break;
            default:
                $value = (string) $value;
        }
        return $value;
    }

    /**
     * Maps JSON data to Entity objects recursively
     *
     * @param array $data
     * @param array $target
     * @param string $class
     * @return array
     */
    public function mapArray(array $data, array $target, string $class) : array
    {
        if (!class_exists($class)) {
            return [];
        }

        foreach ($data as $key => $value) {
            $entity = new $class;
            $refClass = new \ReflectionClass($entity);
            $classProperties = $refClass->getProperties(\ReflectionProperty::IS_PUBLIC);
            foreach ($classProperties as $property) {
                $propertyName = $property->getName();
                $jsonPropertyName = $this->getJsonPropertyName($propertyName);
                if (!array_key_exists($jsonPropertyName, $value)) {
                    continue;
                }
                $annotations = self::parseAnnotations($property->getDocComment());
                list($type, $isArrayProp) = self::inspectProperty($annotations);
                if ($isArrayProp) {
                    $type = $refClass->getNamespaceName() . "\\" . $type;
                    $entity->$propertyName = $this->mapArray($value[$jsonPropertyName], $entity->$propertyName, $type);
                } else {
                    $propValue = $this->validate($value[$jsonPropertyName], $annotations);
                    $entity->$propertyName = self::valueToType($type, $propValue);
                }
            }
            $target[$key] = $entity;
        }

        return $target;
    }

    protected function getJsonPropertyName(string $propertyName) : string
    {
        if (array_key_exists($propertyName, self::NAMES_MAP)) {
            $propertyName = self::NAMES_MAP[$propertyName];
        }
        return $propertyName;
    }

    protected function validate($value, array $annotations)
    {
        if (!array_key_exists('validator', $annotations)) {
            return $value;
        }

        $validatorClass = self::VALIDATORS_NS . $annotations['validator'][0];
        if (class_exists($validatorClass)) {
            /** @var $validator ValidatorInterface **/
            $validator = new $validatorClass($value);
            $value = $validator->isValid() ? $value : $validator->validationErrorMessage();
            unset($validator);
        }

        return $value;
    }
}