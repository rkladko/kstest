<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 12.07.17
 * Time: 17:58
 */

namespace KeepSolid\TestTask\Service;


use KeepSolid\TestTask\Entity\Hotel;

class PartnerService implements PartnerServiceInterface
{
    const DATASOURCE = __DIR__ . '/../../data/15475.json';

    /**
     * This method should read from a datasource (JSON in our case)
     * and return an unsorted list of hotels found in the datasource.
     *
     * @param int $cityId
     *
     * @return \KeepSolid\TestTask\Entity\Hotel[]
     */
    public function getResultForCityId(int $cityId): array
    {
        $jsonString = @file_get_contents(self::DATASOURCE);
        if ($jsonString === false) {
            throw new \RuntimeException("File not found " . self::DATASOURCE);
        }

        $city = json_decode($jsonString, true);
        if ($city === null) {
            throw new \RuntimeException("Cannot parse " . self::DATASOURCE);
        }

        if ($city['id'] != $cityId) {
            throw new \InvalidArgumentException("$cityId not found in datasource");
        }

        $mapper = new JsonMapperService();
        $hotels = $mapper->mapArray($city['hotels'], [], Hotel::class);

        return $hotels;
    }
}