<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 13.07.17
 * Time: 13:23
 */

namespace KeepSolid\TestTask\Service;


class IoCContainerService implements IoCContainerInterface
{
    protected $bindings = [];

    public function register(string $name, \Closure $resolver)
    {
        unset($this->bindings[$name]);
        $this->bindings[$name] = $resolver;
    }

    public function resolve(string $name)
    {
        if (!array_key_exists($name, $this->bindings)) {
            return null;
        }

        return call_user_func($this->bindings[$name]);
    }

}