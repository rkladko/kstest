<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 13.07.17
 * Time: 12:45
 */

namespace KeepSolid\TestTask\Service;


interface IoCContainerInterface
{
    public function register(string $name, \Closure $resolver);
    public function resolve(string $name);
}