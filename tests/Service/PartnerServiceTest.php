<?php
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 12.07.17
 * Time: 18:24
 */

namespace KeepSolid\TestTask\Service;

use KeepSolid\TestTask\Entity\{Hotel, Partner, Price};

/**
 * @package KeepSolid\TestTask\Service
 * @coversDefaultClass \KeepSolid\TestTask\Service\PartnerService
 */
class PartnerServiceTest extends \PHPUnit_Framework_TestCase
{
    const DUESSELDORF_ID = 15475;

    public function test_getResultForCityId_with_city_duesseldorf()
    {
        $partnerService = new PartnerService();
        $hotelResult = $partnerService->getResultForCityId(self::DUESSELDORF_ID);

        $this->assertInternalType('array', $hotelResult);
        $this->assertCount(2, $hotelResult);

        $this->assertArrayHasKey('1', $hotelResult);
        $hotel = $hotelResult['1'];
        $this->assertInstanceOf(Hotel::class, $hotel);

        $this->assertArrayHasKey('101', $hotel->partners);
        $partner = $hotel->partners['101'];
        $this->assertInstanceOf(Partner::class, $partner);

        $this->assertArrayHasKey('1001', $partner->prices);
        $price = $partner->prices['1001'];
        $this->assertInstanceOf(Price::class, $price);
    }
}
