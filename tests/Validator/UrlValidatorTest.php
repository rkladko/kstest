<?php
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 13.07.17
 * Time: 9:12
 */

namespace Validator;

use KeepSolid\TestTask\Validator\UrlValidator;

class UrlValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider urlProvider
     * @param string $url
     * @param bool $expected
     */
    public function test_url(string $url, bool $expected)
    {
         $this->assertEquals((new UrlValidator($url))->isValid(), $expected);
    }

    public function urlProvider()
    {
        return [
            ['http://www.booking.com', true],
            ['https://www.booking.com', true],
            ['http://www.booking.com:8000/test?q=test', true],
            ['www.booking.com', false],
            ['booking.com', false],
            ['bookingcom', false],
        ];
    }
}
